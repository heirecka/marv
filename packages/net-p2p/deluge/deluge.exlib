# Copyright 2009, 2011 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools blacklist=3 multibuild=false ]

SUMMARY="BitTorrent client with a client/server model"
HOMEPAGE="http://www.deluge-torrent.org/"
DOWNLOADS="http://download.deluge-torrent.org/source/${PNV}.tar.bz2"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="
    gtk libnotify webinterface

    gtk          [[ description = [ Install GTK based client for the deluge daemon ] ]]
    webinterface [[ description = [ Allows controlling the deluge daemon from web ] ]]

    libnotify [[ requires = gtk ]]
"

# GTK and webinterface are automagic
DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
    build+run:
        dev-python/chardet[python_abis:*(-)?]
        dev-python/pyopenssl[python_abis:*(-)?]
        dev-python/pyxdg[python_abis:*(-)?]
        net-p2p/libtorrent-rasterbar[>=0.16.0][python] [[
            description = [ 0.16 is required because of fixing patch and api incompatibility ]
        ]]
        net-twisted/Twisted[>=8.1][python_abis:*(-)?]
        gtk? (
            dev-python/dbus-python[python_abis:*(-)?]
            gnome-bindings/pygtk:2[>=2.12][glade][python_abis:*(-)?]
            gnome-desktop/librsvg:2
            x11-apps/xdg-utils
            libnotify? ( dev-python/notify-python[python_abis:*(-)?] )
        )
        webinterface? ( dev-python/Mako[python_abis:*(-)?] )
"

BUGS_TO="marv@exherbo.org"

UPSTREAM_CHANGELOG="http://dev.deluge-torrent.org/wiki/ChangeLog"
UPSTREAM_DOCUMENTATION="http://dev.deluge-torrent.org/wiki/UserGuide"
UPSTREAM_RELEASE_NOTES="http://dev.deluge-torrent.org/wiki/ReleaseNotes"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/0001-GTKUI-Fix-keyerror-showing-prefs.patch"
)

